package com.softengunina.ec17;

import com.softengunina.ec17.Entity.Utente;
import org.junit.Test;
import static org.junit.Assert.*;

public class UtenteUnitTest {

    @Test
    public void testUtente1() {
        String result = Utente.convertPasswordToMd5(null);
        String expectedResult = null;
        assertEquals(expectedResult, result);
    }

    @Test
    public void testUtente2() {
        String result = Utente.convertPasswordToMd5("");
        String expectedResult = "d41d8cd98f00b204e9800998ecf8427e";
        assertEquals(expectedResult, result);
    }

    @Test
    public void testUtente3() {
        String result = Utente.convertPasswordToMd5("123");
        String expectedResult = "202cb962ac59075b964b07152d234b70";
        assertEquals(expectedResult, result);
    }

    @Test
    public void testUtente4() {
        String result = Utente.convertPasswordToMd5("abc");
        String expectedResult = "900150983cd24fb0d6963f7d28e17f72";
        assertEquals(expectedResult, result);
    }

    @Test
    public void testUtente5() {
        String result = Utente.convertPasswordToMd5("abc123");
        String expectedResult = "e99a18c428cb38d5f260853678922e03";
        assertEquals(expectedResult, result);
    }

    @Test
    public void testUtente6() {
        String result = Utente.convertPasswordToMd5("aBC123");
        String expectedResult = "fd133327ee356935ca1c00bf2b473554";
        assertEquals(expectedResult, result);
    }

    @Test
    public void testUtente7() {
        String result = Utente.convertPasswordToMd5("abC.123");
        String expectedResult = "9a50d51f05b185996cab03ba7c697539";
        assertEquals(expectedResult, result);
    }

}