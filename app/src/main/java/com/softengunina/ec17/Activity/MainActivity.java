package com.softengunina.ec17.Activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.softengunina.ec17.Entity.Utente;
import com.softengunina.ec17.Fragment.AcquistiFragment;
import com.softengunina.ec17.Fragment.CarrelloFragment;
import com.softengunina.ec17.Fragment.HomeFragment;
import com.softengunina.ec17.Fragment.LoginFragment;
import com.softengunina.ec17.Fragment.ProfiloFragment;
import com.softengunina.ec17.GlobalClass;
import com.softengunina.ec17.R;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);

        if (savedInstanceState == null)
        {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.content_frame, new HomeFragment()).commit();
        }

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(0).setChecked(true);
        navigationView.setNavigationItemSelectedListener(this);

        SharedPreferences prefs = getSharedPreferences("ec17UserData", MODE_PRIVATE);
        final String email = prefs.getString("email","");
        final String password = prefs.getString("password","");

        final GlobalClass globalVariable = (GlobalClass) getApplicationContext();
        RequestQueue mRequestQueue = Volley.newRequestQueue(this);
        Response.Listener<JSONObject> getListener = new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {
                Gson gson=new Gson();
                Utente utente = gson.fromJson(response.toString(), Utente.class);
                if(utente.getPassword().equals(password)){
                    globalVariable.setUtenteLoggato(utente);
                }
                checkUserLoggato(email);
            }
        };
        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError err){
                checkUserLoggato(email);
            }
        };
        JsonObjectRequest request=new JsonObjectRequest("http://3.8.20.158:8080/api/utenti/email?email="+email, null, getListener, errorListener){
            @Override
            public Map<String, String> getHeaders() {
                String credentials = "admin" + ":" + "admin";
                String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Basic " + base64EncodedCredentials);
                return headers;
            }
        };
        mRequestQueue.add(request);
    }


    public void checkUserLoggato(String email){

        GlobalClass globalVariable = (GlobalClass) getApplicationContext();
        View nav_header =  navigationView.getHeaderView(0).findViewById(R.id.nav_header);
        View headerView = navigationView.getHeaderView(0);
        if(!email.equals("") && globalVariable.getUtenteLoggato()!=null){
            TextView nomeNavHeader = headerView.findViewById(R.id.nav_header_nome);
            TextView emailNavHeader = headerView.findViewById(R.id.nav_header_email);
            nomeNavHeader.setText(globalVariable.getUtenteLoggato().getNome() + " " + globalVariable.getUtenteLoggato().getCognome());
            emailNavHeader.setText(globalVariable.getUtenteLoggato().getEmail());

            Menu menuView = navigationView.getMenu();
            menuView.getItem(2).setVisible(true);
            menuView.getItem(3).setVisible(true);

            nav_header.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    changeFragment(ProfiloFragment.class, null);
                    drawer.closeDrawer(GravityCompat.START);
                }
            });
        }else{
            TextView nomeNavHeader = headerView.findViewById(R.id.nav_header_nome);
            TextView emailNavHeader = headerView.findViewById(R.id.nav_header_email);
            nomeNavHeader.setText("Login / Registrazione");
            emailNavHeader.setText("");

            Menu menuView = navigationView.getMenu();
            menuView.getItem(2).setVisible(false);
            menuView.getItem(3).setVisible(false);

            nav_header.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    changeFragment(LoginFragment.class, null);
                    drawer.closeDrawer(GravityCompat.START);
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.homepage_icon) {
            changeFragment(HomeFragment.class, null);
        } else if (id == R.id.cart_icon) {
            changeFragment(CarrelloFragment.class, null);
        } else if (id == R.id.home) {
            drawer.openDrawer(GravityCompat.START);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_carrello) {
            changeFragment(CarrelloFragment.class, null);
        } else if (id == R.id.nav_home) {
            changeFragment(HomeFragment.class, null);
        } else if (id == R.id.nav_acquisti) {
            changeFragment(AcquistiFragment.class, null);
        } else if (id == R.id.nav_logout) {

            GlobalClass globalVariable = (GlobalClass) getApplicationContext();
            globalVariable.setUtenteLoggato(null);

            SharedPreferences prefs = getSharedPreferences("ec17UserData", MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("email", "");
            editor.putString("password", "");
            editor.commit();
            TextView nomeNavHeader = findViewById(R.id.nav_header_nome);
            TextView emailNavHeader = findViewById(R.id.nav_header_email);
            nomeNavHeader.setText("Login / Registrazione");
            emailNavHeader.setText("");

            View nav_header =  navigationView.getHeaderView(0).findViewById(R.id.nav_header);
            nav_header.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    changeFragment(LoginFragment.class, null);
                    drawer.closeDrawer(GravityCompat.START);
                }
            });

            Menu menuView = navigationView.getMenu();
            menuView.getItem(2).setVisible(false);
            menuView.getItem(3).setVisible(false);

            Toast.makeText(this, "Logout eseguito", Toast.LENGTH_SHORT).show();
            changeFragment(HomeFragment.class, null);

        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public <T extends Serializable> void changeFragment(Class fragmentClass, T obj){
        Fragment fragment = null;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            Toast.makeText(this, "Error" + e.getMessage(), Toast.LENGTH_SHORT).show();
            return;
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        Bundle bundle = new Bundle();
        bundle.putSerializable("param", obj);
        fragment.setArguments(bundle);

        transaction.replace(R.id.content_frame, fragment);
        transaction.addToBackStack(null);
        transaction.commit();

        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    //gestione permessi di scrittura/lettura
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }
}
