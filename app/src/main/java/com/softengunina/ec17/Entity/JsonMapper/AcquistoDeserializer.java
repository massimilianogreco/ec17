package com.softengunina.ec17.Entity.JsonMapper;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.softengunina.ec17.Entity.Acquisto;

import java.lang.reflect.Type;
import java.util.UUID;

public class AcquistoDeserializer implements JsonDeserializer<Acquisto> {

    @Override
    public Acquisto deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        if (json == null)
            return null;
        JsonObject jObject = json.getAsJsonObject();
        Acquisto acquisto = new Acquisto();
        acquisto.setId(UUID.fromString(jObject.get("id").getAsString()));
        acquisto.setData(jObject.get("data").getAsString());
        acquisto.setPrezzoTotale(jObject.get("prezzoTotale").getAsDouble());
        return acquisto;
    }
}
