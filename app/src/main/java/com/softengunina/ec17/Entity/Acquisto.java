package com.softengunina.ec17.Entity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.softengunina.ec17.Entity.JsonMapper.AcquistoDeserializer;
import com.softengunina.ec17.Entity.JsonMapper.AcquistoSerializer;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class Acquisto {
    private UUID id;
    private Map<Articolo, Integer> articoli;
    private Utente utente;
    private String data;
    private double prezzoTotale;

    public Acquisto() {
    }

    public Acquisto(UUID id, Map<Articolo, Integer> articoli, Utente utente, String data, double prezzoTotale) {
        this.id = id;
        this.articoli = articoli;
        this.utente = utente;
        this.data = data;
        this.prezzoTotale = prezzoTotale;
    }

    public Acquisto(List<Articolo> carrello, Utente utente) {
        this.utente = utente;
        articoli = new HashMap<>();
        for(Articolo articolo : carrello){
            articoli.put(articolo, articolo.getQuantita());
        }
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Map<Articolo, Integer> getArticoli() {
        return articoli;
    }

    public void setArticoli(Map<Articolo, Integer> articoli) {
        this.articoli = articoli;
    }

    public Utente getUtente() {
        return utente;
    }

    public void setUtente(Utente utente) {
        this.utente = utente;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public double getPrezzoTotale() {
        return prezzoTotale;
    }

    public void setPrezzoTotale(double prezzoTotale) {
        this.prezzoTotale = prezzoTotale;
    }

    @Override
    public String toString() {
        return "Acquisto{" +
                "id=" + id +
                ", articoli=" + articoli +
                ", utente=" + utente +
                ", data='" + data + '\'' +
                ", prezzoTotale=" + prezzoTotale +
                '}';
    }

    public JSONObject serialize(){
        GsonBuilder gson = new GsonBuilder();
        gson.registerTypeAdapter(Acquisto.class, new AcquistoSerializer());
        Gson parser = gson.create();
        try {
            return new JSONObject(parser.toJson(this));
        } catch (JSONException e) {
            return null;
        }
    }

    public static Acquisto deserialize(String json){
        GsonBuilder gsonBldr = new GsonBuilder();
        gsonBldr.registerTypeAdapter(Acquisto.class, new AcquistoDeserializer());
        return gsonBldr.create().fromJson(json, Acquisto.class);
    }
}
