package com.softengunina.ec17.Entity;

import java.io.Serializable;
import java.util.UUID;

public class Articolo implements Serializable {

    private UUID id;
    private String nome;
    private String categoria;
    private double prezzo;
    private String descrizione;
    private int quantita;
    private String foto;

    public Articolo() {
    }

    public Articolo(Articolo articolo, int quantita) {
        this(articolo.id, articolo.nome, articolo.categoria, articolo.prezzo, articolo.descrizione, quantita, articolo.foto);
    }

    public Articolo(UUID id, String nome, String categoria, double prezzo, String descrizione, int quantita, String foto) {
        this.id = id;
        this.nome = nome;
        this.categoria = categoria;
        this.prezzo = prezzo;
        this.descrizione = descrizione;
        this.quantita = quantita;
        this.foto = foto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Articolo articolo = (Articolo) o;
        return id.equals(articolo.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public double getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public int getQuantita() {
        return quantita;
    }

    public void setQuantita(int quantita) {
        this.quantita = quantita;
    }

    public void addQuantita(int quantita) {
        this.quantita += quantita;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    @Override
    public String toString() {
        return "Articolo{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", categoria='" + categoria + '\'' +
                ", prezzo=" + prezzo +
                ", descrizione='" + descrizione + '\'' +
                ", quantita=" + quantita +
                ", foto='" + foto + '\'' +
                '}';
    }
}
