package com.softengunina.ec17.Entity.JsonMapper;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.softengunina.ec17.Entity.Acquisto;
import com.softengunina.ec17.Entity.Articolo;

import java.lang.reflect.Type;

public class AcquistoSerializer implements JsonSerializer<Acquisto> {

    @Override
    public JsonElement serialize(Acquisto acquisto, Type type, JsonSerializationContext context) {
        JsonObject root = new JsonObject();

        JsonObject articoli = new JsonObject();
        for(Articolo articolo : acquisto.getArticoli().keySet()){
            articoli.addProperty(articolo.getId().toString(), acquisto.getArticoli().get(articolo));
        }

        Gson gson = new Gson();
        root.add("articoli", articoli);
        root.add("utente",gson.toJsonTree(acquisto.getUtente()));
        return root;
    }

}
