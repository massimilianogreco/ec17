package com.softengunina.ec17.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.softengunina.ec17.Activity.MainActivity;
import com.softengunina.ec17.Adapter.CarrelloAdapter;
import com.softengunina.ec17.Entity.Articolo;
import com.softengunina.ec17.Entity.Utente;
import com.softengunina.ec17.GlobalClass;
import com.softengunina.ec17.Listener.RecyclerViewClickListener;
import com.softengunina.ec17.Listener.RecyclerViewSpinnerChangeListener;
import com.softengunina.ec17.R;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.HttpStatus;

public class CarrelloFragment extends Fragment {

    private List<Articolo> carrello;
    private Utente utenteLoggato;
    private CarrelloAdapter mAdapter;
    private RequestQueue mRequestQueue;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Carrello");
        carrello = ((GlobalClass) getActivity().getApplicationContext()).getCarrello();
        utenteLoggato = ((GlobalClass) getActivity().getApplicationContext()).getUtenteLoggato();
        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_carrello, container, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.removeItem(R.id.cart_icon);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        NavigationView navigationView = getActivity().findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(1).setChecked(true);

        RecyclerView mRecyclerView = getActivity().findViewById(R.id.carrello_lista_prodotti_view);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        RecyclerViewClickListener recyclerViewClickListener = new RecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position) {
                ((MainActivity)getActivity()).changeFragment(ArticoloFragment.class, carrello.get(position));
            }
        };
        RecyclerViewClickListener recyclerViewButtonClickListener = new RecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position) {
                carrello.remove(position);
                mAdapter.notifyDataSetChanged();
                checkCarrelloVuoto();
            }
        };
        RecyclerViewSpinnerChangeListener recyclerViewSpinnerChangeListener= new RecyclerViewSpinnerChangeListener() {
            @Override
            public void onItemSelected(View view, int position, int quantita) {
                carrello.get(position).setQuantita(quantita+1);
                TextView prezzoView = getActivity().findViewById(R.id.carrello_prezzo);
                prezzoView.setText("Totale " + String.valueOf(getPrezzoTotaleCarrello()) + "€");
            }
        };

        mAdapter = new CarrelloAdapter(carrello, getActivity(), recyclerViewClickListener, recyclerViewButtonClickListener, recyclerViewSpinnerChangeListener);
        mRecyclerView.setAdapter(mAdapter);
        checkCarrelloVuoto();

        Button acquistaButton = getActivity().findViewById(R.id.carrello_acquista_button);
        if(utenteLoggato!=null) {
            acquistaButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) getActivity()).changeFragment(ConfermaFragment.class, null);
                }
            });
        }else{
            acquistaButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getActivity(), "Eseguire il login per completare l'acquisto", Toast.LENGTH_SHORT).show();
                    ((MainActivity) getActivity()).changeFragment(LoginFragment.class, true);
                }
            });
        }

        mRequestQueue = Volley.newRequestQueue(getActivity());
        if(getArguments().getSerializable("param")!=null && (boolean)getArguments().getSerializable("param"))
            aggiornaQuantita();
    }

    public void checkCarrelloVuoto(){
        if(carrello.size()==0) {
            getActivity().findViewById(R.id.carrello_carrello_vuoto).setVisibility(View.VISIBLE);
            getActivity().findViewById(R.id.carrello_acquista_layout).setVisibility(View.GONE);

        }else {
            getActivity().findViewById(R.id.carrello_carrello_vuoto).setVisibility(View.GONE);
            getActivity().findViewById(R.id.carrello_acquista_layout).setVisibility(View.VISIBLE);
            TextView prezzoView = getActivity().findViewById(R.id.carrello_prezzo);
            prezzoView.setText("Totale " + String.valueOf(getPrezzoTotaleCarrello()) + "€");
        }
    }

    private void aggiornaQuantita(){
        for(Articolo articolo : carrello)
            aggiornaQuantitaArticolo(articolo);
    }

    private void aggiornaQuantitaArticolo(final Articolo articoloOld){

        Response.Listener<JSONObject> getListener = new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {
                Gson gson=new Gson();
                Articolo articolo = gson.fromJson(response.toString(), Articolo.class);
                if(articolo.getQuantita()==0)
                    carrello.remove(articoloOld);
                else if(articolo.getQuantita()<articoloOld.getQuantita())
                    articoloOld.setQuantita(articolo.getQuantita());
                mAdapter.notifyDataSetChanged();
                checkCarrelloVuoto();
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError err)
            {
                if (err instanceof ServerError && err.networkResponse.statusCode == HttpStatus.SC_NOT_FOUND)
                    carrello.remove(articoloOld);
                else
                    Toast.makeText(getActivity(), "Problema di connessione", Toast.LENGTH_LONG).show();
                mAdapter.notifyDataSetChanged();
            }
        };

        JsonObjectRequest request=new JsonObjectRequest("http://3.8.20.158:8080/api/articoli/"+articoloOld.getId(), null, getListener, errorListener){
            @Override
            public Map<String, String> getHeaders() {
                String credentials = "admin" + ":" + "admin";
                String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Basic " + base64EncodedCredentials);
                return headers;
            }
        };
        mRequestQueue.add(request);

    }

    public double getPrezzoTotaleCarrello(){
        double prezzoTotale = 0;
        for(Articolo articolo : carrello)
            prezzoTotale += articolo.getPrezzo()*articolo.getQuantita();
        return Math.round(prezzoTotale * 100.0) / 100.0;
    }
}