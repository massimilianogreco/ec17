package com.softengunina.ec17.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.google.gson.Gson;
import com.softengunina.ec17.Activity.MainActivity;
import com.softengunina.ec17.Adapter.ArticoliAdapter;
import com.softengunina.ec17.Entity.Articolo;
import com.softengunina.ec17.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.HttpStatus;

public class HomeFragment extends Fragment {

    private RequestQueue mRequestQueue;
    private ArticoliAdapter mAdapter;
    private List<Articolo> articoli;
    private boolean filtersShowed = false;
    private JsonArrayRequest request;
    private SearchView searchView;

    //filters
    private String quantitaFilter = "";
    private String orderFilter = "";
    private String prezzoMinFilter = "";
    private String prezzoMaxFilter = "";
    private String categoriaFilter = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("EC-17");
        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        NavigationView navigationView = getActivity().findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(0).setChecked(true);

        searchView = getActivity().findViewById(R.id.mSearch);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(mAdapter!=null)
                    mAdapter.getFilter().filter(query);
                return false;
            }
            @Override
            public boolean onQueryTextChange(String query) {
                if(mAdapter!=null)
                    mAdapter.getFilter().filter(query);
                return false;
            }
        });
        mRequestQueue = Volley.newRequestQueue(getActivity());
        RecyclerView mRecyclerView = getActivity().findViewById(R.id.home_lista_articoli);
        GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(),2);
        mRecyclerView.setLayoutManager(mLayoutManager);
        ArticoliAdapter.ArticoloClickListener articoloClickListener = new ArticoliAdapter.ArticoloClickListener() {
            @Override
            public void onClick(Articolo articolo) {
                ((MainActivity) getActivity()).changeFragment(ArticoloFragment.class, articolo);
            }
        };
        articoli = new ArrayList<>();
        mAdapter = new ArticoliAdapter(articoli, getActivity(), articoloClickListener);
        mRecyclerView.setAdapter(mAdapter);

        filterRequest();

        final SwipeRefreshLayout mySwipeRefreshLayout = getActivity().findViewById(R.id.homepage_swipe_refresh);
        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        mySwipeRefreshLayout.setRefreshing(false);
                        mRequestQueue.add(request);
                    }
                }
        );
        final Spinner orderSpinner = getActivity().findViewById(R.id.home_ordina_spinner);
        orderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        orderFilter = "";
                        break;
                    case 1:
                        orderFilter = "orderBy=nome&directionSort=0";
                        break;
                    case 2:
                        orderFilter = "orderBy=nome&directionSort=1";
                        break;
                    case 3:
                        orderFilter = "orderBy=prezzo&directionSort=0";
                        break;
                    case 4:
                        orderFilter = "orderBy=prezzo&directionSort=1";
                        break;
                }
                filterRequest();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        final LinearLayout filters = getActivity().findViewById(R.id.home_filters);
        filters.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int heightFilters = filters.getMeasuredHeight();
        ViewGroup.LayoutParams lp = filters.getLayoutParams();
        lp.height = 0;
        filters.setLayoutParams(lp);
        final Button filtersButton = getActivity().findViewById(R.id.home_filtri_button);
        filtersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!filtersShowed) {
                    ResizeAnimation resizeAnimation = new ResizeAnimation(
                            filters,
                            heightFilters,
                            0
                    );
                    resizeAnimation.setDuration(500);
                    filters.startAnimation(resizeAnimation);
                    filtersShowed = true;
                }else{
                    ResizeAnimation resizeAnimation = new ResizeAnimation(
                            filters,
                            0,
                            heightFilters
                    );
                    resizeAnimation.setDuration(500);
                    filters.startAnimation(resizeAnimation);
                    filtersShowed = false;
                }
            }
        });

        final CrystalRangeSeekbar rangeSeekbar = getActivity().findViewById(R.id.rangeSeekbar1);
        final TextView tvMin = getActivity().findViewById(R.id.home_seekbar_min);
        final TextView tvMax = getActivity().findViewById(R.id.home_seekbar_max);
        rangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                tvMin.setText(String.valueOf(Math.round((double)minValue*100.0)/100.0)+" €");
                tvMax.setText(String.valueOf(Math.round((double)maxValue*100.0)/100.0)+" €");
            }
        });
        if(!prezzoMinFilter.equals(""))
            rangeSeekbar.setMinStartValue(Float.valueOf(prezzoMinFilter.substring(10)));
        if(!prezzoMaxFilter.equals(""))
            rangeSeekbar.setMaxStartValue(Float.valueOf(prezzoMaxFilter.substring(10)));
        rangeSeekbar.apply();

        Button applicaFiltriButton = getActivity().findViewById(R.id.home_applica_filtri_button);
        applicaFiltriButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (orderSpinner.getSelectedItemPosition()){
                    case 0:
                        orderFilter = "";
                        break;
                    case 1:
                        orderFilter = "orderBy=nome&directionSort=0";
                        break;
                    case 2:
                        orderFilter = "orderBy=nome&directionSort=1";
                        break;
                    case 3:
                        orderFilter = "orderBy=prezzo&directionSort=0";
                        break;
                    case 4:
                        orderFilter = "orderBy=prezzo&directionSort=1";
                        break;
                }

                Switch disponibiliSwitch = getActivity().findViewById(R.id.home_disponibili_switch);
                if(disponibiliSwitch.isChecked())
                    quantitaFilter = "quantitaMin=1";
                else
                    quantitaFilter = "quantitaMin=0";

                prezzoMinFilter = "prezzoMin=" + String.valueOf(rangeSeekbar.getSelectedMinValue());
                prezzoMaxFilter = "prezzoMax=" + String.valueOf(rangeSeekbar.getSelectedMaxValue());

                Spinner categoriaSpinner = getActivity().findViewById(R.id.home_categoria_spinner);
                if(categoriaSpinner.getSelectedItemPosition() != 0)
                    categoriaFilter = "categoria=" + categoriaSpinner.getSelectedItem().toString();
                else
                    categoriaFilter = "";

                filtersButton.performClick();
                filterRequest();
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.removeItem(R.id.homepage_icon);
    }

    private Response.Listener<JSONArray> getListener = new Response.Listener<JSONArray>()
    {
        @Override
        public void onResponse(JSONArray response) {
            List<Articolo> articoliNew = new ArrayList<>();
            Gson gson=new Gson();
            for (int i=0; i<response.length(); i++)
            {
                try {
                    JSONObject j=response.getJSONObject(i);
                    Articolo articolo =gson.fromJson(j.toString(),Articolo.class);
                    articoliNew.add(articolo);
                }
                catch (JSONException e) {
                    return;
                }
            }
            articoli.clear();
            articoli.addAll(articoliNew);
            mAdapter.notifyDataSetChanged();
            if(searchView.getQuery() != "")
                mAdapter.getFilter().filter(searchView.getQuery());
        }
    };

    private Response.ErrorListener errorListener = new Response.ErrorListener()
    {
        @Override
        public void onErrorResponse(VolleyError err)
        {
            if (err instanceof ServerError && err.networkResponse.statusCode == HttpStatus.SC_NOT_FOUND)
                Toast.makeText(getActivity(), "Nessun articolo trovato", Toast.LENGTH_LONG).show();
            else
                Toast.makeText(getActivity(), "Errore, problema di connessione", Toast.LENGTH_LONG).show();
        }
    };

    private void filterRequest(){
        String filters = "";
        if(!orderFilter.equals(""))
            filters += orderFilter + "&";
        if(!quantitaFilter.equals(""))
            filters += quantitaFilter + "&";
        if(!prezzoMinFilter.equals(""))
            filters += prezzoMinFilter + "&";
        if(!prezzoMaxFilter.equals(""))
            filters += prezzoMaxFilter + "&";
        if(!categoriaFilter.equals(""))
            filters += categoriaFilter + "&";
        if(!filters.equals("")) {
            filters = filters.replaceAll(".$", "");
            filters = "?" + filters;
        }
        request = new JsonArrayRequest("http://3.8.20.158:8080/api/articoli/filter"+filters, getListener, errorListener){
            @Override
            public Map<String, String> getHeaders() {
                String credentials = "admin" + ":" + "admin";
                String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Basic " + base64EncodedCredentials);
                return headers;
            }
        };
        mRequestQueue.add(request);
    }

    public class ResizeAnimation extends Animation {
        final int targetHeight;
        View view;
        int startHeight;

        public ResizeAnimation(View view, int targetHeight, int startHeight) {
            this.view = view;
            this.targetHeight = targetHeight;
            this.startHeight = startHeight;
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            int newHeight = (int) (startHeight+(targetHeight - startHeight) * interpolatedTime);
            //to support decent animation, change new heigt as Nico S. recommended in comments
            //int newHeight = (int) (startHeight+(targetHeight - startHeight) * interpolatedTime);
            view.getLayoutParams().height = newHeight;
            view.requestLayout();
        }

        @Override
        public void initialize(int width, int height, int parentWidth, int parentHeight) {
            super.initialize(width, height, parentWidth, parentHeight);
        }

        @Override
        public boolean willChangeBounds() {
            return true;
        }
    }
}