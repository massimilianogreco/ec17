package com.softengunina.ec17.Fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.softengunina.ec17.Activity.MainActivity;
import com.softengunina.ec17.Entity.Utente;
import com.softengunina.ec17.GlobalClass;
import com.softengunina.ec17.R;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.HttpStatus;

import static android.content.Context.MODE_PRIVATE;

public class LoginFragment extends Fragment {

    private boolean redirectAcquisto = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Login");
        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        if(getArguments().getSerializable("param")!=null)
            redirectAcquisto=(boolean)getArguments().getSerializable("param");
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        NavigationView navigationView = getActivity().findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(0).setChecked(false);
        navigationView.getMenu().getItem(1).setChecked(false);

        Button btnRegistrati = getActivity().findViewById(R.id.login_registrati_button);
        btnRegistrati.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).changeFragment(RegistrazioneFragment.class, null);
            }
        });

        Button btnLogin = getActivity().findViewById(R.id.login_login_button);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView emailText = getActivity().findViewById(R.id.login_email);
                TextView passwordText = getActivity().findViewById(R.id.login_password);
                final TextInputLayout emailTextLayout = getActivity().findViewById(R.id.login_email_layout);
                final TextInputLayout passwordTextLayout = getActivity().findViewById(R.id.login_password_layout);
                String email = emailText.getText().toString().trim().toLowerCase();
                String password = passwordText.getText().toString().trim();
                boolean error = false;
                emailText.addTextChangedListener(new TextWatcher() {
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                    public void onTextChanged(CharSequence s, int start, int before, int count) {}
                    public void afterTextChanged(Editable s) {
                        emailTextLayout.setError(null);
                    }
                });
                passwordText.addTextChangedListener(new TextWatcher() {
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                    public void onTextChanged(CharSequence s, int start, int before, int count) {}
                    public void afterTextChanged(Editable s) {
                        passwordTextLayout.setError(null);
                    }
                });
                if(TextUtils.isEmpty(emailText.getText())) {
                    emailTextLayout.setError("Inserire un email");
                    error = true;
                }
                else if(!email.contains("@") || !email.contains(".")) {
                    emailTextLayout.setError("Email non valida");
                    error = true;
                }
                if(TextUtils.isEmpty(passwordText.getText())) {
                    passwordTextLayout.setError("Inserire la password");
                    error = true;
                }
                if(!error)
                    eseguiAccesso(email, password);
            }
        });
    }

    private void eseguiAccesso(String email, final String password){

        RequestQueue mRequestQueue = Volley.newRequestQueue(getActivity());

        Response.Listener<JSONObject> getListener = new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {

                Gson gson=new Gson();
                Utente utente = gson.fromJson(response.toString(), Utente.class);
                if(!utente.getPassword().equals(Utente.convertPasswordToMd5(password)))
                    Toast.makeText(getActivity(), "L’e-mail e la password inserite non corrispondono", Toast.LENGTH_LONG).show();
                else{
                    SharedPreferences prefs = getActivity().getSharedPreferences("ec17UserData", MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("email", utente.getEmail());
                    editor.putString("password", utente.getPassword());
                    editor.commit();
                    loginEseguito(utente);

                    Toast.makeText(getActivity(), "Accesso eseguito" , Toast.LENGTH_LONG).show();
                    if(redirectAcquisto)
                        ((MainActivity)getActivity()).changeFragment(ConfermaFragment.class, null);
                    else
                        ((MainActivity)getActivity()).changeFragment(HomeFragment.class, null);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError err)
            {
                if (err instanceof ServerError && err.networkResponse.statusCode == HttpStatus.SC_NOT_FOUND)
                    Toast.makeText(getActivity(), "Non esiste un account con quell'indirizzo e-mail", Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(getActivity(), "Problema di connessione", Toast.LENGTH_LONG).show();
            }
        };

        JsonObjectRequest request=new JsonObjectRequest("http://3.8.20.158:8080/api/utenti/email?email="+email, null, getListener, errorListener){
            @Override
            public Map<String, String> getHeaders() {
                String credentials = "admin" + ":" + "admin";
                String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Basic " + base64EncodedCredentials);
                return headers;
            }
        };
        mRequestQueue.add(request);

    }

    private void loginEseguito(Utente utente){
        GlobalClass globalVariable = (GlobalClass) getActivity().getApplicationContext();
        globalVariable.setUtenteLoggato(utente);

        TextView nomeNavHeader = getActivity().findViewById(R.id.nav_header_nome);
        TextView emailNavHeader = getActivity().findViewById(R.id.nav_header_email);
        nomeNavHeader.setText(utente.getNome() + " " + utente.getCognome());
        emailNavHeader.setText(utente.getEmail());

        NavigationView navigationView = getActivity().findViewById(R.id.nav_view);
        View nav_header =  navigationView.getHeaderView(0).findViewById(R.id.nav_header);
        final DrawerLayout drawer = getActivity().findViewById(R.id.drawer_layout);
        nav_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).changeFragment(ProfiloFragment.class, null);
                drawer.closeDrawer(GravityCompat.START);
            }
        });

        Menu menuView = navigationView.getMenu();
        menuView.getItem(2).setVisible(true);
        menuView.getItem(3).setVisible(true);
    }
}