package com.softengunina.ec17.Fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.softengunina.ec17.Entity.Articolo;
import com.softengunina.ec17.GlobalClass;
import com.softengunina.ec17.R;

public class ArticoloFragment extends Fragment {

    private Articolo articolo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Articolo");
        setHasOptionsMenu(true);
        articolo=(Articolo)getArguments().getSerializable("param");
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_articolo, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        NavigationView navigationView = getActivity().findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(0).setChecked(false);
        navigationView.getMenu().getItem(1).setChecked(false);

        TextView textViewNomeArticolo = getActivity().findViewById(R.id.articolo_nome);
        TextView textViewPrezzoArticolo = getActivity().findViewById(R.id.articolo_prezzo);
        TextView textViewDescrizione = getActivity().findViewById(R.id.articolo_descrizione);
        ImageView imageViewImgArticolo = getActivity().findViewById(R.id.articolo_immagine);
        textViewNomeArticolo.setText(articolo.getNome());
        textViewDescrizione.setText("Descrizione: \n" + articolo.getDescrizione());
        textViewPrezzoArticolo.setText("Prezzo: " + String.valueOf(articolo.getPrezzo()) + "€");
        Glide.with(this)
                .load(articolo.getFoto())
                .into(imageViewImgArticolo);

        final Spinner quantitaSpinner = getActivity().findViewById(R.id.articolo_quantita);
        Button aggiungiButton = getActivity().findViewById(R.id.articolo_aggiungi_button);
        TextView disponibileText= getActivity().findViewById(R.id.articolo_disponibile_text);

        if(articolo.getQuantita()==0){
            quantitaSpinner.setEnabled(false);
            aggiungiButton.setEnabled(false);
            disponibileText.setVisibility(View.VISIBLE);
        }

        aggiungiButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GlobalClass globalVariable = (GlobalClass) getActivity().getApplicationContext();

                int quantita = Integer.valueOf(quantitaSpinner.getSelectedItem().toString());
                int res = globalVariable.getCarrello().indexOf(articolo);
                if(res==-1) {
                    if(quantita>articolo.getQuantita()){
                        showDialog();
                        globalVariable.getCarrello().add(new Articolo(articolo, articolo.getQuantita()));
                    }else {
                        globalVariable.getCarrello().add(new Articolo(articolo, quantita));
                        Toast.makeText(getActivity(), "Articolo/i aggiunto/i al carrello", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    if(globalVariable.getCarrello().get(res).getQuantita() + quantita > articolo.getQuantita()) {
                        showDialog();
                        globalVariable.getCarrello().get(res).setQuantita(articolo.getQuantita());
                    }else {
                        globalVariable.getCarrello().get(res).addQuantita(quantita);
                        Toast.makeText(getActivity(), "Articolo/i aggiunto/i al carrello", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void showDialog(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getContext(), android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getContext());
        }
        builder.setTitle("Articolo terminato")
                .setMessage("Sono state aggiunte al carrello tutte le unità disponibili per questo articolo.")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .show();
    }
}