package com.softengunina.ec17.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.softengunina.ec17.Activity.MainActivity;
import com.softengunina.ec17.Entity.Utente;
import com.softengunina.ec17.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegistrazioneFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Registrazione");
        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_registrazione, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        NavigationView navigationView = getActivity().findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(0).setChecked(false);
        navigationView.getMenu().getItem(1).setChecked(false);

        Button buttonRegistrati = getActivity().findViewById(R.id.registrazione_registrati_button);
        buttonRegistrati.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TextView nomeText = getActivity().findViewById(R.id.registrazione_nome);
                TextView cognomeText = getActivity().findViewById(R.id.registrazione_cognome);
                TextView emailText = getActivity().findViewById(R.id.registrazione_email);
                TextView telText = getActivity().findViewById(R.id.registrazione_tel);
                TextView indirizzoText = getActivity().findViewById(R.id.registrazione_indirizzo);
                TextView passwordText = getActivity().findViewById(R.id.registrazione_password);

                final TextInputLayout nomeTextLayout = getActivity().findViewById(R.id.registrazione_nome_layout);
                final TextInputLayout cognomeTextLayout = getActivity().findViewById(R.id.registrazione_cognome_layout);
                final TextInputLayout emailTextLayout = getActivity().findViewById(R.id.registrazione_email_layout);
                final TextInputLayout telTextLayout = getActivity().findViewById(R.id.registrazione_tel_layout);
                final TextInputLayout indirizzoTextLayout = getActivity().findViewById(R.id.registrazione_indirizzo_layout);
                final TextInputLayout passwordTextLayout = getActivity().findViewById(R.id.registrazione_password_layout);

                String email = emailText.getText().toString().toLowerCase();

                boolean error = false;

                if(TextUtils.isEmpty(nomeText.getText())) {
                    nomeTextLayout.setError("Inserire il nome");
                    error = true;
                }
                if(TextUtils.isEmpty(cognomeText.getText())) {
                    cognomeTextLayout.setError("Inserire il cognome");
                    error = true;
                }
                if(TextUtils.isEmpty(emailText.getText())) {
                    emailTextLayout.setError("Inserire un email");
                    error = true;
                }
                else if(!email.contains("@") || !email.contains(".")) {
                    emailTextLayout.setError("Email non valida");
                    error = true;
                }
                if(TextUtils.isEmpty(telText.getText())) {
                    telTextLayout.setError("Inserire un numero di telefono");
                    error = true;
                }
                if(TextUtils.isEmpty(indirizzoText.getText())) {
                    indirizzoTextLayout.setError("Inserire l'indirizzo");
                    error = true;
                }
                if(TextUtils.isEmpty(passwordText.getText())) {
                    passwordTextLayout.setError("Inserire la password");
                    error = true;
                }

                nomeText.addTextChangedListener(new TextWatcher() {
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                    public void onTextChanged(CharSequence s, int start, int before, int count) {}
                    public void afterTextChanged(Editable s) {
                        nomeTextLayout.setError(null);
                    }
                });
                cognomeText.addTextChangedListener(new TextWatcher() {
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                    public void onTextChanged(CharSequence s, int start, int before, int count) {}
                    public void afterTextChanged(Editable s) {
                        cognomeTextLayout.setError(null);
                    }
                });
                emailText.addTextChangedListener(new TextWatcher() {
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                    public void onTextChanged(CharSequence s, int start, int before, int count) {}
                    public void afterTextChanged(Editable s) {
                        emailTextLayout.setError(null);
                    }
                });
                telText.addTextChangedListener(new TextWatcher() {
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                    public void onTextChanged(CharSequence s, int start, int before, int count) {}
                    public void afterTextChanged(Editable s) {
                        telTextLayout.setError(null);
                    }
                });
                indirizzoText.addTextChangedListener(new TextWatcher() {
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                    public void onTextChanged(CharSequence s, int start, int before, int count) {}
                    public void afterTextChanged(Editable s) {
                        indirizzoTextLayout.setError(null);
                    }
                });
                passwordText.addTextChangedListener(new TextWatcher() {
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                    public void onTextChanged(CharSequence s, int start, int before, int count) {}
                    public void afterTextChanged(Editable s) {
                        passwordTextLayout.setError(null);
                    }
                });
                System.out.println(passwordText.getText().toString().trim());
                if(!error)
                    eseguiRegistrazione(new Utente(
                            nomeText.getText().toString().trim(),
                            cognomeText.getText().toString().trim(),
                            email.trim(),
                            telText.getText().toString().trim(),
                            indirizzoText.getText().toString().trim(),
                            Utente.convertPasswordToMd5(passwordText.getText().toString().trim())));
            }
        });
    }

    private void eseguiRegistrazione(Utente utente){

        RequestQueue mRequestQueue = Volley.newRequestQueue(getActivity());
        Gson gson = new Gson();
        JSONObject jsonUtente;
        try {
            jsonUtente = new JSONObject(gson.toJson(utente));
        } catch (JSONException e) {
            Toast.makeText(getActivity(), "Errore in fase di registrazione", Toast.LENGTH_SHORT).show();
            return;
        }

        Response.Listener<JSONObject> postListener = new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {
                Toast.makeText(getActivity(), "Registrazione completata", Toast.LENGTH_LONG).show();
                ((MainActivity)getActivity()).changeFragment(HomeFragment.class, null);
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError err)
            {
                TextInputLayout emailTextLayout = getActivity().findViewById(R.id.registrazione_email_layout);
                emailTextLayout.setError("Email già utilizzata");
            }
        };

        JsonObjectRequest request=new JsonObjectRequest("http://3.8.20.158:8080/api/utenti", jsonUtente, postListener, errorListener){
            @Override
            public Map<String, String> getHeaders() {
                String credentials = "admin" + ":" + "admin";
                String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Basic " + base64EncodedCredentials);
                return headers;
            }
        };
        mRequestQueue.add(request);
    }
}