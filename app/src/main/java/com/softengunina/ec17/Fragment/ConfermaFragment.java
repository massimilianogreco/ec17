package com.softengunina.ec17.Fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.PayPal;
import com.braintreepayments.api.exceptions.InvalidArgumentException;
import com.braintreepayments.api.interfaces.PaymentMethodNonceCreatedListener;
import com.braintreepayments.api.models.PayPalRequest;
import com.braintreepayments.api.models.PaymentMethodNonce;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.softengunina.ec17.Activity.MainActivity;
import com.softengunina.ec17.Entity.Acquisto;
import com.softengunina.ec17.Entity.Articolo;
import com.softengunina.ec17.Entity.Utente;
import com.softengunina.ec17.GlobalClass;
import com.softengunina.ec17.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpStatus;

public class ConfermaFragment extends Fragment {

    private GlobalClass globalVariable;
    private List<Articolo> carrello;
    private Utente utenteLoggato;
    private String token;
    private BraintreeFragment braintreeFragment;
    private ProgressDialog progress;
    private double totPagamento;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Dati di spedizione");
        globalVariable = (GlobalClass) getActivity().getApplicationContext();
        carrello = globalVariable.getCarrello();
        utenteLoggato = globalVariable.getUtenteLoggato();
        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_conferma, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        NavigationView navigationView = getActivity().findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(0).setChecked(false);
        navigationView.getMenu().getItem(1).setChecked(false);

        TextView nomeView = getActivity().findViewById(R.id.conferma_nome);
        TextView cognomeView = getActivity().findViewById(R.id.conferma_cognome);
        TextView telView = getActivity().findViewById(R.id.conferma_tel);
        TextView indirizzoView = getActivity().findViewById(R.id.conferma_indirizzo);

        nomeView.setText(utenteLoggato.getNome());
        cognomeView.setText(utenteLoggato.getCognome());
        telView.setText(utenteLoggato.getTelefono());
        indirizzoView.setText(utenteLoggato.getIndirizzo());

        progress = new ProgressDialog(getActivity());
        progress.setMessage("Paypal");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);

        totPagamento = getPrezzoTotaleCarrello();

        Button confermaButton = getActivity().findViewById(R.id.conferma_button);
        confermaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkAndPay();
            }
        });

        getToken();
    }

    private void getToken(){
        String credentials = "admin" + ":" + "admin";
        String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Authorization", "Basic " + base64EncodedCredentials);
        client.get("http://3.8.20.158:8080/api/paypal/token", new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String clientToken) {
                token = clientToken;
                configuraPaypalFragment();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getActivity(), "Errore, problema di connessione", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void configuraPaypalFragment(){
        try {
            braintreeFragment = BraintreeFragment.newInstance(getActivity(), token);
        } catch (InvalidArgumentException e) {
            return;
        }
        braintreeFragment.addListener(new PaymentMethodNonceCreatedListener() {
            @Override
            public void onPaymentMethodNonceCreated(PaymentMethodNonce paymentMethodNonce) {
                sendNonceToServer(paymentMethodNonce.getNonce(), totPagamento);
            }
        });
    }

    private void checkAndPay(){
        RequestQueue mRequestQueue = Volley.newRequestQueue(getActivity());

        Gson gson = new Gson();
        JSONArray articoli = null;
        try {
            articoli = new JSONArray(gson.toJson(carrello));
        } catch (JSONException e) {
            return;
        }

        final Response.Listener<JSONArray> patchListener = new Response.Listener<JSONArray>(){
            @Override
            public void onResponse(JSONArray response) {
                effettuaPagamento();
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError err)
            {
                if (err instanceof ServerError && err.networkResponse.statusCode == HttpStatus.SC_NOT_FOUND){
                    showDialog();
                    ((MainActivity)getActivity()).changeFragment(CarrelloFragment.class, true);
                }else if (err instanceof ParseError)    //empty response -> ok
                    patchListener.onResponse(null);
            }
        };

        JsonArrayRequest request=new JsonArrayRequest(Request.Method.PATCH,"http://3.8.20.158:8080/api/articoli/riduciquantita", articoli, patchListener, errorListener){
            @Override
            public Map<String, String> getHeaders() {
                String credentials = "admin" + ":" + "admin";
                String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Basic " + base64EncodedCredentials);
                return headers;
            }
        };
        mRequestQueue.add(request);
    }

    private void effettuaPagamento(){
        progress.show();
        PayPal.authorizeAccount(braintreeFragment);
        PayPalRequest request = new PayPalRequest(String.valueOf(totPagamento))
                .currencyCode("EUR")
                .intent(PayPalRequest.INTENT_AUTHORIZE);
        PayPal.requestOneTimePayment(braintreeFragment, request);
    }

    private void sendNonceToServer(String nonce, Double amount) {
        AsyncHttpClient client = new AsyncHttpClient();
        String credentials = "admin" + ":" + "admin";
        String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
        client.addHeader("Authorization", "Basic " + base64EncodedCredentials);
        RequestParams params = new RequestParams();
        params.put("nonce", nonce);
        params.put("amount", amount);
        client.post("http://3.8.20.158:8080/api/paypal/checkout", params,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        progress.hide();
                        salvaAcquisto(new Acquisto(carrello,utenteLoggato));
                        globalVariable.svuotaCarrello();
                        ((MainActivity) getActivity()).changeFragment(HomeFragment.class, null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        progress.hide();
                        annullaAcquisto();
                        Toast.makeText(getActivity(), "Pagamento fallito, acquisto annullato", Toast.LENGTH_LONG).show();
                        ((MainActivity) getActivity()).changeFragment(HomeFragment.class, null);
                    }
                }
        );
    }

    private void salvaAcquisto(Acquisto acquisto){
        RequestQueue mRequestQueue = Volley.newRequestQueue(getActivity());

        Response.Listener<JSONObject> postListener = new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {
                Toast.makeText(getActivity(), "Acquisto completato", Toast.LENGTH_LONG).show();
                ((MainActivity)getActivity()).changeFragment(HomeFragment.class, null);
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError err)
            {
                Toast.makeText(getActivity(), "Acquisto fallito", Toast.LENGTH_LONG).show();
            }
        };

        JsonObjectRequest request=new JsonObjectRequest("http://3.8.20.158:8080/api/acquisti", acquisto.serialize(), postListener, errorListener){
            @Override
            public Map<String, String> getHeaders() {
                String credentials = "admin" + ":" + "admin";
                String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Basic " + base64EncodedCredentials);
                return headers;
            }
        };
        mRequestQueue.add(request);
    }

    private void annullaAcquisto(){
        RequestQueue mRequestQueue = Volley.newRequestQueue(getActivity());

        Gson gson = new Gson();
        JSONArray articoli = null;
        try {
            articoli = new JSONArray(gson.toJson(carrello));
        } catch (JSONException e) {
            return;
        }

        JsonArrayRequest request=new JsonArrayRequest(Request.Method.PATCH, "http://3.8.20.158:8080/api/articoli/rifornisciquantita", articoli, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {}
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {}
        }){
            @Override
            public Map<String, String> getHeaders() {
                String credentials = "admin" + ":" + "admin";
                String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Basic " + base64EncodedCredentials);
                return headers;
            }
        };
        mRequestQueue.add(request);
    }

    private double getPrezzoTotaleCarrello(){
        double prezzoTotale = 0;
        for(Articolo articolo : carrello)
            prezzoTotale += articolo.getPrezzo()*articolo.getQuantita();
        return Math.round(prezzoTotale * 100.0) / 100.0;
    }

    private void showDialog(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getContext(), android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getContext());
        }
        builder.setTitle("Articolo/i non disponibile/i")
                .setMessage("Uno o più articoli non sono più disponibili, il carrello è stato modificato in base alle disponibilità.")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .show();
    }
}