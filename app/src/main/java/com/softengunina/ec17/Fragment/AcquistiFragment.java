package com.softengunina.ec17.Fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.softengunina.ec17.Activity.MainActivity;
import com.softengunina.ec17.Adapter.AcquistiAdapter;
import com.softengunina.ec17.Entity.Acquisto;
import com.softengunina.ec17.Entity.Utente;
import com.softengunina.ec17.GlobalClass;
import com.softengunina.ec17.Listener.RecyclerViewClickListener;
import com.softengunina.ec17.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.HttpStatus;

public class AcquistiFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerViewClickListener recyclerViewClickListener;
    private List<Acquisto> acquisti;
    private Utente utenteLoggato;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Acquisti");
        setHasOptionsMenu(true);
        utenteLoggato = ((GlobalClass) getActivity().getApplicationContext()).getUtenteLoggato();
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_acquisti, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final RequestQueue mRequestQueue = Volley.newRequestQueue(getActivity());
        final JsonArrayRequest request=new JsonArrayRequest("http://3.8.20.158:8080/api/acquisti/utente/" + utenteLoggato.getId(), getListener, errorListener){
            @Override
            public Map<String, String> getHeaders() {
                String credentials = "admin" + ":" + "admin";
                String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Basic " + base64EncodedCredentials);
                return headers;
            }
        };
        mRequestQueue.add(request);

        mRecyclerView = getActivity().findViewById(R.id.acquisti_lista_acquisti_view);
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        recyclerViewClickListener = new RecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position) {
                scaricaEVisualizzaFattura(acquisti.get(position));
            }
        };

        final SwipeRefreshLayout mySwipeRefreshLayout = getActivity().findViewById(R.id.acquisti);
        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        mySwipeRefreshLayout.setRefreshing(false);
                        mRequestQueue.add(request);
                    }
                }
        );
    }

    private Response.Listener<JSONArray> getListener = new Response.Listener<JSONArray>()
    {
        @Override
        public void onResponse(JSONArray response) {
            acquisti = new ArrayList<>();
            Gson gson=new Gson();
            for (int i=0; i<response.length(); i++)
            {
                try {
                    JSONObject j=response.getJSONObject(i);
                    Acquisto acquisto = Acquisto.deserialize(j.toString());
                    acquisti.add(acquisto);
                }
                catch (JSONException e) {
                    Toast.makeText(getActivity(), "Errore, problema di connessione", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
            AcquistiAdapter mAdapter = new AcquistiAdapter(acquisti/*, getActivity()*/, recyclerViewClickListener);
            mRecyclerView.setAdapter(mAdapter);
            getActivity().findViewById(R.id.acquiti_nessun_acquisto).setVisibility(View.GONE);
        }
    };

    private Response.ErrorListener errorListener = new Response.ErrorListener()
    {
        @Override
        public void onErrorResponse(VolleyError err)
        {
            if (err instanceof ServerError && err.networkResponse.statusCode == HttpStatus.SC_NOT_FOUND)
                getActivity().findViewById(R.id.acquiti_nessun_acquisto).setVisibility(View.VISIBLE);
            else
                Toast.makeText(getActivity(), "Errore, problema di connessione", Toast.LENGTH_LONG).show();
        }
    };

    private void scaricaEVisualizzaFattura(Acquisto acquisto){

        MainActivity.verifyStoragePermissions(getActivity());

        final RequestQueue mRequestQueue = Volley.newRequestQueue(getActivity());
        final JsonObjectRequest request=new JsonObjectRequest("http://3.8.20.158:8080/api/acquisti/fattura/"+acquisto.getId(), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String fileContentsEncoded = response.getString("fattura");
                    byte[] fileContents = Base64.decode(fileContentsEncoded, Base64.DEFAULT);

                    String filename = "Fattura EC-17.pdf";
                    FileOutputStream outputStream;
                    File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), filename);

                    outputStream = new FileOutputStream(file);
                    outputStream.write(fileContents);
                    outputStream.close();

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(file), "application/pdf");
                    startActivity(intent);
                    Toast.makeText(getActivity(), "Fattura salvata in download", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Toast.makeText(getActivity(), "Errore durante il salvataggio della fattura", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "Errore durante il download della fattura", Toast.LENGTH_SHORT).show();
                error.printStackTrace();
            }
        }){
            @Override
            public Map<String, String> getHeaders() {
                String credentials = "admin" + ":" + "admin";
                String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Basic " + base64EncodedCredentials);
                return headers;
            }
        };
        mRequestQueue.add(request);
    }
}