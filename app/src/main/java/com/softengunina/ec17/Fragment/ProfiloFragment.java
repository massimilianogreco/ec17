package com.softengunina.ec17.Fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.softengunina.ec17.Activity.MainActivity;
import com.softengunina.ec17.Entity.Utente;
import com.softengunina.ec17.GlobalClass;
import com.softengunina.ec17.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static android.content.Context.MODE_PRIVATE;

public class ProfiloFragment extends Fragment {

    private GlobalClass globalVariable;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Profilo");
        globalVariable = (GlobalClass) getActivity().getApplicationContext();
        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profilo, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        NavigationView navigationView = getActivity().findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(0).setChecked(false);
        navigationView.getMenu().getItem(1).setChecked(false);
        navigationView.getMenu().getItem(2).setChecked(false);

        final TextView nomeText = getActivity().findViewById(R.id.profilo_nome);
        final TextView cognomeText = getActivity().findViewById(R.id.profilo_cognome);
        final TextView emailText = getActivity().findViewById(R.id.profilo_email);
        final TextView telText = getActivity().findViewById(R.id.profilo_tel);
        final TextView indirizzoText = getActivity().findViewById(R.id.profilo_indirizzo);
        final TextView passwordText = getActivity().findViewById(R.id.profilo_password);

        final TextInputLayout nomeTextLayout = getActivity().findViewById(R.id.profilo_nome_layout);
        final TextInputLayout cognomeTextLayout = getActivity().findViewById(R.id.profilo_cognome_layout);
        final TextInputLayout emailTextLayout = getActivity().findViewById(R.id.profilo_email_layout);
        final TextInputLayout telTextLayout = getActivity().findViewById(R.id.profilo_tel_layout);
        final TextInputLayout indirizzoTextLayout = getActivity().findViewById(R.id.profilo_indirizzo_layout);
        final TextInputLayout passwordTextLayout = getActivity().findViewById(R.id.profilo_password_layout);

        final Utente utente = globalVariable.getUtenteLoggato();

        nomeText.setText(utente.getNome());
        cognomeText.setText(utente.getCognome());
        emailText.setText(utente.getEmail());
        telText.setText(utente.getTelefono());
        indirizzoText.setText(utente.getIndirizzo());

        Button buttonRegistrati = getActivity().findViewById(R.id.profilo_salva_button);
        buttonRegistrati.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = emailText.getText().toString();
                String password = passwordText.getText().toString();

                boolean error = false;

                if(TextUtils.isEmpty(nomeText.getText())) {
                    nomeTextLayout.setError("Inserire il nome");
                    error = true;
                }
                if(TextUtils.isEmpty(cognomeText.getText())) {
                    cognomeTextLayout.setError("Inserire il cognome");
                    error = true;
                }
                if(TextUtils.isEmpty(emailText.getText())) {
                    emailTextLayout.setError("Inserire un email");
                    error = true;
                }
                else if(!email.contains("@") || !email.contains(".")) {
                    emailTextLayout.setError("Email non valida");
                    error = true;
                }
                if(TextUtils.isEmpty(telText.getText())) {
                    telTextLayout.setError("Inserire un numero di telefono");
                    error = true;
                }
                if(TextUtils.isEmpty(indirizzoText.getText())) {
                    indirizzoTextLayout.setError("Inserire l'indirizzo");
                    error = true;
                }

                nomeText.addTextChangedListener(new TextWatcher() {
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                    public void onTextChanged(CharSequence s, int start, int before, int count) {}
                    public void afterTextChanged(Editable s) {
                        nomeTextLayout.setError(null);
                    }
                });
                cognomeText.addTextChangedListener(new TextWatcher() {
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                    public void onTextChanged(CharSequence s, int start, int before, int count) {}
                    public void afterTextChanged(Editable s) {
                        cognomeTextLayout.setError(null);
                    }
                });
                emailText.addTextChangedListener(new TextWatcher() {
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                    public void onTextChanged(CharSequence s, int start, int before, int count) {}
                    public void afterTextChanged(Editable s) {
                        emailTextLayout.setError(null);
                    }
                });
                telText.addTextChangedListener(new TextWatcher() {
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                    public void onTextChanged(CharSequence s, int start, int before, int count) {}
                    public void afterTextChanged(Editable s) {
                        telTextLayout.setError(null);
                    }
                });
                indirizzoText.addTextChangedListener(new TextWatcher() {
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                    public void onTextChanged(CharSequence s, int start, int before, int count) {}
                    public void afterTextChanged(Editable s) {
                        indirizzoTextLayout.setError(null);
                    }
                });
                passwordText.addTextChangedListener(new TextWatcher() {
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                    public void onTextChanged(CharSequence s, int start, int before, int count) {}
                    public void afterTextChanged(Editable s) {
                        passwordTextLayout.setError(null);
                    }
                });

                String securePassword = null;
                if(!password.equals(""))
                    securePassword = Utente.convertPasswordToMd5(password);
                if(!error)
                    modificaProfilo(new Utente(
                            utente.getId(),
                            nomeText.getText().toString(),
                            cognomeText.getText().toString(),
                            emailText.getText().toString(),
                            telText.getText().toString(),
                            indirizzoText.getText().toString(),
                            securePassword));
            }
        });
    }

    private void modificaProfilo(final Utente utente){

        RequestQueue mRequestQueue = Volley.newRequestQueue(getActivity());
        Gson gson = new Gson();
        JSONObject jsonUtente;
        try {
            jsonUtente = new JSONObject(gson.toJson(utente));
        } catch (JSONException e) {
            Toast.makeText(getActivity(), "Errore in fase di modifica", Toast.LENGTH_SHORT).show();
            return;
        }

        Response.Listener<JSONObject> putListener = new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {
                aggiornaDatiLocaliUtente(utente.getId());
                Toast.makeText(getActivity(), "Modifica completata", Toast.LENGTH_LONG).show();
                ((MainActivity)getActivity()).changeFragment(HomeFragment.class, null);
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError err){
                Toast.makeText(getActivity(), "Errore in fase di modifica", Toast.LENGTH_SHORT).show();
            }
        };

        JsonObjectRequest request=new JsonObjectRequest(Request.Method.PUT, "http://3.8.20.158:8080/api/utenti/" + utente.getId().toString(), jsonUtente, putListener, errorListener){
            @Override
            public Map<String, String> getHeaders() {
                String credentials = "admin" + ":" + "admin";
                String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Basic " + base64EncodedCredentials);
                return headers;
            }
        };
        mRequestQueue.add(request);
    }

    private void aggiornaDatiLocaliUtente(UUID idUtente){

        RequestQueue mRequestQueue = Volley.newRequestQueue(getActivity());

        Response.Listener<JSONObject> getListener = new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {
                Gson gson = new Gson();
                Utente utente = gson.fromJson(response.toString(),Utente.class);
                globalVariable.setUtenteLoggato(utente);
                SharedPreferences prefs = getActivity().getSharedPreferences("ec17UserData", MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("password", utente.getPassword());
                editor.commit();

                TextView nomeNavHeader = getActivity().findViewById(R.id.nav_header_nome);
                nomeNavHeader.setText(utente.getNome() + " " + utente.getCognome());
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError err){
                Toast.makeText(getActivity(), "Errore aggiornamento dati", Toast.LENGTH_SHORT).show();
            }
        };

        JsonObjectRequest request=new JsonObjectRequest("http://3.8.20.158:8080/api/utenti/" + idUtente, null, getListener, errorListener){
            @Override
            public Map<String, String> getHeaders() {
                String credentials = "admin" + ":" + "admin";
                String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Basic " + base64EncodedCredentials);
                return headers;
            }
        };
        mRequestQueue.add(request);
    }
}