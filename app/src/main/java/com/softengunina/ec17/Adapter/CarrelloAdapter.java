package com.softengunina.ec17.Adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.softengunina.ec17.Entity.Articolo;
import com.softengunina.ec17.Listener.RecyclerViewClickListener;
import com.softengunina.ec17.Listener.RecyclerViewSpinnerChangeListener;
import com.softengunina.ec17.R;

import java.util.List;

public class CarrelloAdapter extends RecyclerView.Adapter<CarrelloAdapter.ViewHolder> {

    private List<Articolo> mDataset;
    private FragmentActivity mFragment;
    private RecyclerViewClickListener rowClickListener;
    private RecyclerViewClickListener buttonClickListener;
    private RecyclerViewSpinnerChangeListener spinnerChangeListener;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView nomeArticolo;
        private Spinner quantita;
        private ImageView image;
        private TextView prezzo;
        private RecyclerViewClickListener rowClickListener;
        private RecyclerViewClickListener buttonClickListener;
        private RecyclerViewSpinnerChangeListener spinnerChangeListener;
        private boolean spinnerChanged;

        public ViewHolder(View v, RecyclerViewClickListener rowListener, RecyclerViewClickListener buttonListener, RecyclerViewSpinnerChangeListener spinnerListener)
        {
            super(v);
            nomeArticolo = v.findViewById(R.id.row_carrello_nome_articolo);
            quantita = v.findViewById(R.id.row_carrello_quantita);
            image = v.findViewById(R.id.row_image);
            prezzo = v.findViewById(R.id.row_carrello_prezzo);
            v.setOnClickListener(this);
            rowClickListener = rowListener;
            buttonClickListener = buttonListener;
            spinnerChangeListener = spinnerListener;

            Button rimuovi = v.findViewById(R.id.row_carrello_rimuovi_articolo);
            rimuovi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    buttonClickListener.onClick(v, getAdapterPosition());
                }
            });

            quantita.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if(spinnerChanged)
                        spinnerChangeListener.onItemSelected(view, getAdapterPosition(), position);
                    spinnerChanged = true;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

        @Override
        public void onClick(View v) {
            rowClickListener.onClick(v, getAdapterPosition());
        }
    }

    public CarrelloAdapter(List<Articolo> mDataset, FragmentActivity mFragment, RecyclerViewClickListener rowClickListener, RecyclerViewClickListener buttonClickListener, RecyclerViewSpinnerChangeListener spinnerChangeListener) {
        this.mDataset = mDataset;
        this.mFragment = mFragment;
        this.rowClickListener = rowClickListener;
        this.buttonClickListener = buttonClickListener;
        this.spinnerChangeListener = spinnerChangeListener;
    }

    @Override
    public CarrelloAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_carrello, parent, false);
        ViewHolder vh = new ViewHolder(v, rowClickListener, buttonClickListener, spinnerChangeListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        Articolo tmp = mDataset.get(position);
        holder.nomeArticolo.setText(tmp.getNome());
        holder.prezzo.setText(String.valueOf(tmp.getPrezzo()) + "€");
        holder.quantita.setSelection(tmp.getQuantita()-1);
        holder.spinnerChanged = false;
        Glide.with(mFragment)
                .load(tmp.getFoto())
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}