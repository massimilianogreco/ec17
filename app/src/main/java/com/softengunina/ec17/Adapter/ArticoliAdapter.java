package com.softengunina.ec17.Adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.softengunina.ec17.Entity.Articolo;
import com.softengunina.ec17.R;

import java.util.ArrayList;
import java.util.List;

public class ArticoliAdapter extends RecyclerView.Adapter<ArticoliAdapter.ViewHolder> implements Filterable {

    public interface ArticoloClickListener {
        void onClick(Articolo articolo);
    }

    private List<Articolo> articoli;
    private List<Articolo> articoliFiltered;
    private FragmentActivity mFragment;
    private ArticoloClickListener mListener;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView nome;
        public TextView prezzo;
        public ImageView image;
        private ArticoloClickListener mListener;

        public ViewHolder(View v, ArticoloClickListener listener)
        {
            super(v);
            nome = v.findViewById(R.id.row_nome);
            prezzo = v.findViewById(R.id.row_prezzo);
            image= v.findViewById(R.id.row_image);
            v.setOnClickListener(this);
            mListener = listener;
        }

        @Override
        public void onClick(View v) {
            mListener.onClick(articoliFiltered.get(getAdapterPosition()));
        }
    }

    public ArticoliAdapter(List<Articolo> articoli, FragmentActivity mFragment, ArticoloClickListener mListener) {
        this.articoli = articoli;
        this.articoliFiltered = articoli;
        this.mFragment = mFragment;
        this.mListener = mListener;
    }

    @Override
    public ArticoliAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_home, parent, false);
        ViewHolder vh = new ViewHolder(v, mListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        Articolo tmp = articoliFiltered.get(position);
        holder.nome.setText(tmp.getNome());
        holder.prezzo.setText(String.valueOf(tmp.getPrezzo()) + "€");
        Glide.with(mFragment)
                .load(tmp.getFoto())
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        return articoliFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty())
                    articoliFiltered = articoli;
                else {
                    List<Articolo> filteredList = new ArrayList<>();
                    for (Articolo articolo : articoli) {
                        if (articolo.getNome().toLowerCase().contains(charString.toLowerCase()) || articolo.getDescrizione().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(articolo);
                        }
                    }
                    articoliFiltered = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = articoliFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                articoliFiltered = (ArrayList<Articolo>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}