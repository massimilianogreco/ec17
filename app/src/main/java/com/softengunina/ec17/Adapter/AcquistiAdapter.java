package com.softengunina.ec17.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.softengunina.ec17.Entity.Acquisto;
import com.softengunina.ec17.Listener.RecyclerViewClickListener;
import com.softengunina.ec17.R;

import java.util.List;

public class AcquistiAdapter extends RecyclerView.Adapter<AcquistiAdapter.ViewHolder> {

    private List<Acquisto> mDataset;
    private RecyclerViewClickListener buttonClickListener;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView cod;
        private TextView prezzo;
        private TextView data;
        private RecyclerViewClickListener buttonClickListener;

        public ViewHolder(View v, RecyclerViewClickListener buttonListener)
        {
            super(v);
            cod = v.findViewById(R.id.row_acquisti_cod_acquisto);
            prezzo = v.findViewById(R.id.row_acquisti_prezzo);
            data = v.findViewById(R.id.row_acquisti_data);
            buttonClickListener = buttonListener;

            Button rimuovi = v.findViewById(R.id.row_acquisti_visualizza_fattura);
            rimuovi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    buttonClickListener.onClick(v, getAdapterPosition());
                }
            });
        }
    }

    public AcquistiAdapter(List<Acquisto> mDataset, RecyclerViewClickListener buttonClickListener) {
        this.mDataset = mDataset;
        this.buttonClickListener = buttonClickListener;
    }

    @Override
    public AcquistiAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_acquisti, parent, false);
        ViewHolder vh = new ViewHolder(v, buttonClickListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        Acquisto tmp = mDataset.get(position);
        holder.cod.setText(tmp.getId().toString());
        holder.prezzo.setText(String.valueOf(tmp.getPrezzoTotale()) + " €");
        holder.data.setText(tmp.getData());
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}