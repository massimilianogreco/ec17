package com.softengunina.ec17;

import android.app.Application;

import com.softengunina.ec17.Entity.Articolo;
import com.softengunina.ec17.Entity.Utente;

import java.util.ArrayList;
import java.util.List;

public class GlobalClass extends Application {

    private List<Articolo> carrello;
    private Utente utenteLoggato;

    public GlobalClass() {
        carrello = new ArrayList<>();
    }

    public List<Articolo> getCarrello() {
        return carrello;
    }

    public void svuotaCarrello(){
        carrello.clear();
    }

    public Utente getUtenteLoggato() {
        return utenteLoggato;
    }

    public void setUtenteLoggato(Utente utenteLoggato) {
        this.utenteLoggato = utenteLoggato;
    }
}
