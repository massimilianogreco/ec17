package com.softengunina.ec17.Listener;

import android.view.View;

public interface RecyclerViewSpinnerChangeListener {
    void onItemSelected(View view, int position, int quantita);
}
