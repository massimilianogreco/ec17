package com.softengunina.ec17.Listener;

import android.view.View;

public interface RecyclerViewClickListener {
    void onClick(View view, int position);
}
